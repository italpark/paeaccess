import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MensajeAutoevaluacionPendienteComponent } from './mensaje-autoevaluacion-pendiente.component';

describe('MensajeAutoevaluacionPendienteComponent', () => {
  let component: MensajeAutoevaluacionPendienteComponent;
  let fixture: ComponentFixture<MensajeAutoevaluacionPendienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeAutoevaluacionPendienteComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MensajeAutoevaluacionPendienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
