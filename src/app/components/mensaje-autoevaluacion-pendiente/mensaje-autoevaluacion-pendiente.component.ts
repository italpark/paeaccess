import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { UserDataService } from 'src/app/providers/user-data.service';

@Component({
  selector: 'app-mensaje-autoevaluacion-pendiente',
  templateUrl: './mensaje-autoevaluacion-pendiente.component.html',
  styleUrls: ['./mensaje-autoevaluacion-pendiente.component.scss'],
})
export class MensajeAutoevaluacionPendienteComponent {
  nombreUsuario: string;


  constructor(
    public router: Router,
    public user: UserDataService
  ) {

  }

  ngOnInit() {

  }
  ngAfterViewInit() {
    this.getNombreUsuario();
  }

  iniciarEvaluacion() {
    let navigationExtras: NavigationExtras = {
      state: {
        tipoEvaluacion: 'autoEvaluacion'
      }
    };
    this.router.navigate(['/evaluacion', navigationExtras]);

  }

  logout() { 
    this.user.logout();
  }

  getNombreUsuario(){
    this.user.getNombreUsuario().then((nombre) => {
      this.nombreUsuario = nombre.toLowerCase();
      console.log(this.nombreUsuario)
      
    })
  }
}
