import { Component, OnInit, Input } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { UserDataService } from 'src/app/providers/user-data.service';

@Component({
  selector: 'app-mensaje-bienvenida',
  templateUrl: './mensaje-bienvenida.component.html',
  styleUrls: ['./mensaje-bienvenida.component.scss'],
})
export class MensajeBienvenidaComponent implements OnInit {

  @Input() nombre: string;
  nombreUsuario:string;
  
  constructor(public storage: Storage,
              public router: Router,
              public user: UserDataService) { }

  ngOnInit() {
    this.getNombreUsuario();

  }
  ngAfterViewInit() {
  }
  iniciarEvaluacion() {
    
    this.router.navigate(['/evaluacion']);

  }
  logout(){ // creo que no se usa
    this.user.logout();
  }
  getNombreUsuario(){
    this.user.getNombreUsuario().then((nombre) => {
      this.nombreUsuario = nombre.toLowerCase();
      
      
    })
  }
}
