import { Component, OnInit, Input, OnChanges, ApplicationRef } from '@angular/core';
import { UserDataService } from 'src/app/providers/user-data.service';
import * as moment from 'moment';

@Component({
  selector: 'app-mensaje-resultado',
  templateUrl: './mensaje-resultado.component.html',
  styleUrls: ['./mensaje-resultado.component.scss'],
})
export class MensajeResultadoComponent implements OnInit {

  @Input() nombre: string;
  @Input() numeroDNI: number;
  nombreUsuario = '';
  fechaAutoEvaluacion: any;
  resultadoAutoEvaluacion: number;
  renderPage = false;
  classBackgroundColor = '';
  mensajeResultado: any = {};
  constructor(
    public user: UserDataService,
    public appRef: ApplicationRef
  ) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.obtenerDatosAutoEvaluacion();

  }

  obtenerDatosAutoEvaluacion() {


    console.log(this.numeroDNI)
    console.log('NUMERO DNI DE MSJ RESULT')
    this.getFechaAutoEvaluacion();
    this.getResultadoAutoEvaluacion();
    this.getNombreUsuario().then(data=>{
      this.renderPage = true;
      console.log(this.nombreUsuario)
    })


  }
  getResultadoAutoEvaluacion() {
    this.user.getValorAutoEvaluacion(this.numeroDNI).then((resultado) => {
      console.log(resultado)
      this.resultadoAutoEvaluacion = resultado;
      if (this.resultadoAutoEvaluacion < 1) {
        this.classBackgroundColor = 'green';
        this.mensajeResultado = { 'titulo': 'Éxito', 'primerRenglon': 'Comuníquese con salud ocupacional antes de concurrir al trabajo' }
        
      } else if (this.resultadoAutoEvaluacion < 3) {
        this.classBackgroundColor = 'yellow';
        this.mensajeResultado = { 'titulo': 'Síntomas de riesgo', 'primerRenglon': 'Comuníquese con salud', 'segundoRenglon': 'ocupacional antes de concurrir al trabajo' }

      } else {
        this.classBackgroundColor = 'red';
        this.mensajeResultado = { 'titulo': 'Quédese en su casa', 'primerRenglon': 'y comuníquese con salud ocupacional' }
      }
      console.log(this.resultadoAutoEvaluacion)

    })
  }
  getFechaAutoEvaluacion() {
    this.user.getFechaAutoEvaluacion(this.numeroDNI).then((fecha) => {
      let fechaDisplay = moment(fecha).format('DD/MM')
      this.fechaAutoEvaluacion = fechaDisplay;
    })
  }
  getNombreUsuario(): Promise<any> {
   return this.user.getNombreUsuario().then((nombre) => {
      this.nombreUsuario = nombre.toLowerCase();
      console.log(this.nombreUsuario)

    })
  }
  logout() { // creo que no se usa
    this.user.logout();
  }

}
