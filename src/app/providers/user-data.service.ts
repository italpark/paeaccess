import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { datosLogin } from '../interfaces/interfaces';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  DATOS_SITUACION_SALUD = 'datosSituacionSalud';
  VALOR_SITUACION_SALUD = 'valorSituacionSalud';
  IS_SIGNED_UP = 'isSignedUp';
  NUMERO_DNI = 'numeroDNI';
  MAYOR_DE_EDAD = 'mayorEdad';
  HAS_LOGGED_IN = 'hasLoggedIn';
  NOMBRE_USUARIO = "nombreUsuario";
  RESULTADO_AUTO_EVALUACION = "resultadoAutoEvaluacion";
  FECHA_AUTO_EVALUACION = 'fechaAutoEvaluacion';
  _datosUsuario: datosLogin;
  constructor(
    public storage: Storage,
    public router: Router
  ) { }


  checkIsSignedUp(): Promise<string> {
    return this.storage.get(this.NUMERO_DNI).then((value) => {
      return value;
    });
  }

  isLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  }

  login(): Promise<any> {
    // await this.setNumeroDNI(numeroDNI);
    // await this.setNombreUsuario(nombreUsuario);
    return this.storage.set(this.HAS_LOGGED_IN, true).then(() => {
      return window.dispatchEvent(new CustomEvent('user:login'));
    });
  }

  logout() {
    this.storage.remove(this.NUMERO_DNI).then(() => {
      this.storage.remove(this.HAS_LOGGED_IN).then(() => {
        return this.storage.remove(this.NUMERO_DNI); // remover dni
      }).then(() => {
        this.router.navigate(["/login", { replaceUrl: true }]);
      });
    })
  }



  setNumeroDNI(numeroDNI: number): Promise<any> {
    return this.storage.set(this.NUMERO_DNI, numeroDNI);
  }
  getNumeroDNI(): Promise<number> {
    return this.storage.get(this.NUMERO_DNI).then((value) => {
      console.log("getnumeroDNI")

      return value;
    });
  }
  setMayorDeEdad(mayor: boolean): Promise<any> {
    return this.storage.set(this.MAYOR_DE_EDAD, mayor);
  }
  getMayorDeEdad(): Promise<number> {
    return this.storage.get(this.MAYOR_DE_EDAD).then((value) => {
      console.log("getnumeroDNI")

      return value;
    });
  }
  getKeys(): Promise<any> {
    return this.storage.keys().then((keys) => {
      console.log("llaves")
      console.log(keys)
      return keys;
    });
  }
  setNombreUsuario(nombre: string): Promise<any> {
    console.log('seteo nombreuser')
    return this.storage.set(this.NOMBRE_USUARIO, nombre);
  }
  getNombreUsuario(): Promise<string> {
    return this.storage.get(this.NOMBRE_USUARIO).then((nombre) => {
      console.log("getNombreUsuario")
      console.log(nombre)
      return nombre;
    });
  }


  //Datos Situacion Salud
  setValorAutoEvaluacion(resultadoAutoEvaluacion: number, numeroDNI: number): Promise<any> {
    return this.storage.set(this.RESULTADO_AUTO_EVALUACION + numeroDNI, resultadoAutoEvaluacion);
  }

  getValorAutoEvaluacion(numeroDNI: number): Promise<number> {
    return this.storage.get(this.RESULTADO_AUTO_EVALUACION + numeroDNI).then((value) => {
      return value;
    });
  }

  borrarAutoEvaluacion(numeroDNI: number): Promise<any> {
    return this.storage.remove(this.RESULTADO_AUTO_EVALUACION + numeroDNI);
  }


  SetFechaAutoEvaluacion(fecha: Date, numeroDNI: number): Promise<any> {
    return this.storage.set(this.FECHA_AUTO_EVALUACION + numeroDNI, fecha);
  }

  getFechaAutoEvaluacion(numeroDNI: number): Promise<Date> {
    return this.storage.get(this.FECHA_AUTO_EVALUACION + numeroDNI).then((value) => {
      return value;
    });
  }

  setDatosSituacionSalud(datosSituacionSalud: datosLogin): Promise<any> {
    return this.storage.set(this.DATOS_SITUACION_SALUD + datosSituacionSalud.numeroDNI, datosSituacionSalud);
  }

  getDatosSituacionSalud(numeroDNI: number): Promise<datosLogin> {
    return this.storage.get(this.DATOS_SITUACION_SALUD + numeroDNI).then((value) => {
      return value;
    });
  }

  //Valor Situacion Salud
  setValorSituacionSalud(valorSituacionSalud: number, numeroDNI: number): Promise<any> {
    return this.storage.set(this.VALOR_SITUACION_SALUD + numeroDNI, valorSituacionSalud);
  }


  getValorSituacionSalud(numeroDNI: number): Promise<number> {
    return this.storage.get(this.VALOR_SITUACION_SALUD + numeroDNI).then((value) => {
      return value;
    });
  }


}


// checkLoginStatus() {
//   return this.userData.isLoggedIn().then(loggedIn => {
//     return this.updateLoggedInStatus(loggedIn);
//   });
// }