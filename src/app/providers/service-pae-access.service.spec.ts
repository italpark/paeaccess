import { TestBed } from '@angular/core/testing';

import { ServicePaeAccessService } from './service-pae-access.service';

describe('ServicePaeAccessService', () => {
  let service: ServicePaeAccessService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicePaeAccessService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
