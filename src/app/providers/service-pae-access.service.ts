import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServicePaeAccessService {

  public versionApp = "2.0";
  public servicioEndpoint = 'https://www.dokoit.com/apps/paeaccess/apiv0_1/servicios';

  constructor(public http: HttpClient) { }


  verificarDNI($userInputArray, $fecha): Promise<any> {
    let url: string = this.servicioEndpoint + '/verificarDNI.php';
    let elements:Array<any>=[{}];
    

    let body = new FormData();
    body.append('token', localStorage.getItem("token"));
    
    body.append('fecha',$fecha);
    for(let i=0;i < $userInputArray.length; i++){
      elements[i]=$userInputArray[i];

    }
    body.append('userInput',  JSON.stringify(elements));


    return this.http.post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  login(credentials) {

    let url: string = this.servicioEndpoint + '/login.php';
    //console.log(credentials)
    let body = new FormData();
    body.append('dni', credentials.numeroDNI);
    body.append('apellido', credentials.apellido);
    body.append('nombre', credentials.nombre);
    body.append('fechaNacimiento', credentials.fechaNacimiento);
    body.append('fcmToken', localStorage.getItem('fcmToken'));
    body.append('versionApp',this.versionApp);
    return this.http.post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }
 

    //'Borrowed' from //https://angular.io/docs/ts/latest/guide/server-communication.html
    private extractData(res: Response) {
      //Convert the response to JSON format
      //console.log(res)
      // let body = res.json();
      //Return the data (or nothing)
      return res || {};
    }
    
    //'Borrowed' from //https://angular.io/docs/ts/latest/guide/server-communication.html
    private handleError(res: Response | any) {
      return Promise.reject(res.message || res);
    }

}
