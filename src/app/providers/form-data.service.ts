import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {
  data: any;
  constructor(public http: HttpClient) { }
  load(): any {
    if (this.data) {
      console.log('no carga')
      return of(this.data);
    } else {
      console.log('carga')

      return this.http
        .get('assets/data/formularios.json');
    }
  }

}
