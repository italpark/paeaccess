import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicePaeAccessService } from './service-pae-access.service';
@Injectable({
  providedIn: 'root'
})
export class ServicepaeaccessService {
  // http://www.dokoit.com/apps/paeaccess/apiv0_1/servicios/obtenercodigo.php

  servicioEndpoint: string;

  constructor(
    public service: ServicePaeAccessService,
    public http: HttpClient) {
    this.servicioEndpoint = service.servicioEndpoint;
    console.log(this.servicioEndpoint)
  }


  obtenerCodigo(fechaEvaluacion, valorResultado) {

    console.log(localStorage.getItem("token"))
    let url: string = this.servicioEndpoint + '/login.php';
    //console.log(credentials)
    let body = new FormData();
    body.append('token', localStorage.getItem("token"));
    body.append('fechaEvaluacion', fechaEvaluacion);
    body.append('valorResultado', valorResultado);

    return this.http.post(url, body)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  //'Borrowed' from //https://angular.io/docs/ts/latest/guide/server-communication.html
  private extractData(res: Response) {
    //Convert the response to JSON format
    //console.log(res)
    // let body = res.json();
    //Return the data (or nothing)
    return res || {};
  }

  //'Borrowed' from //https://angular.io/docs/ts/latest/guide/server-communication.html
  private handleError(res: Response | any) {
    return Promise.reject(res.message || res);
  }
}
