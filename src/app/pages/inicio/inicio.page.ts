import { Component, OnInit } from '@angular/core';
import { UserDataService } from 'src/app/providers/user-data.service';
import { datosLogin } from 'src/app/interfaces/interfaces';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  respondioEvaluacion: boolean = false;
  registroCompleto: boolean = false;
  valorSituacionSalud: Number;
  datosLogin: datosLogin;
  numeroDNI: number;
  tipoEvaluacion: string;
  renderPage: boolean;
  constructor(
    public user: UserDataService,
    public router: Router
  ) { }


  ngOnInit() { // TODO: usar Async/await?
    // this.getValorSituacionSalud(); // Si tengo dato situacion salud presento autotest
    console.log('this.valorSituacionSalud')
    this.getDatosSituacionSalud();
    this.obtenerEstadoStorage();


  }

  ngAfterViewInit() {
    // this.getNumeroDNI();

    this.user.getKeys(); // debugging

  }

  ionViewWillEnter() {
    this.ngOnInit();
  }

  obtenerEstadoStorage() {
    this.user.getNumeroDNI().then((valor) => {
      this.numeroDNI = valor;
      console.log(this.numeroDNI)
      this.user.getValorSituacionSalud(this.numeroDNI).then((valor) => {

        this.valorSituacionSalud = valor;
        console.log(this.valorSituacionSalud)

        if (this.valorSituacionSalud != null) { // Si tiene valor base ya se registro
          this.user.getFechaAutoEvaluacion(this.numeroDNI).then(fecha => {

            let fechaEvaluacion = moment(fecha);

            let fechaActual = moment(); // "2020-05-31", "YYYY-MM-DD"

            if (fechaActual.diff(fechaEvaluacion, 'days') >= 2) { // si la ultima autoEvaluacion fue hace 2 dias la borro
              this.respondioEvaluacion = false;
              this.user.borrarAutoEvaluacion(this.numeroDNI);
            } else { // si no obtengo el valor
              this.user.getValorAutoEvaluacion(this.numeroDNI).then((valorAutoEvaluacion) => {
                if (valorAutoEvaluacion != null) {
                  this.respondioEvaluacion = true;
                }
              })
            }
          })
          this.registroCompleto = true;

        } else {
          this.registroCompleto = false;

        }

      });
      this.renderPage = true;

    },err=>{ // Si no tiene cordova viene aka
      console.log(err);
      this.user.logout();
    });
  }
  ionViewDidEnter() {

    // let number = this.user.getValorSituacionSalud()
    //console.log("iniciopagets")



  }


  getValorSituacionSalud() {  // Valor base para el resultado final


  }

  getDatosSituacionSalud() {

    this.user.getDatosSituacionSalud(this.numeroDNI).then((valor) => {
      this.datosLogin = valor;
      console.log('this.datosLogin')
      console.log(this.datosLogin)
    })

  }

  logout() {
    this.user.logout();

  }
}
