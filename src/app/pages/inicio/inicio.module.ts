import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InicioPageRoutingModule } from './inicio-routing.module';

import { InicioPage } from './inicio.page';
import { MensajeBienvenidaComponent } from 'src/app/components/mensaje-bienvenida/mensaje-bienvenida.component';
import { MensajeAutoevaluacionPendienteComponent } from 'src/app/components/mensaje-autoevaluacion-pendiente/mensaje-autoevaluacion-pendiente.component';
import { MensajeResultadoComponent } from 'src/app/components/mensaje-resultado/mensaje-resultado.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InicioPageRoutingModule
  ],
  declarations: [InicioPage, MensajeBienvenidaComponent, MensajeAutoevaluacionPendienteComponent, MensajeResultadoComponent]
})
export class InicioPageModule {}
