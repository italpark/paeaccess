import { Component, OnInit, ViewChild } from '@angular/core';
import { FormDataService } from 'src/app/providers/form-data.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UserDataService } from 'src/app/providers/user-data.service';
import { IonSlides, AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { ServicepaeaccessService } from 'src/app/providers/servicepaeaccess.service';

//TODO IMPORTANTE: respuestaMultiple esta invertido . SituacionSalud es datosUsuario
//

@Component({
  selector: 'app-evaluacion',
  templateUrl: './evaluacion.page.html',
  styleUrls: ['./evaluacion.page.scss'],
})
export class EvaluacionPage implements OnInit {

  progressBarValue: number = 0;
  preguntasRespondidas: number = 0;
  cantidadPreguntas: number = 0;
  respuestas: any = [];
  autoEvaluacion: boolean;
  valorBaseAutoEvaluacion: number;
  nombreUsuario: string;
  numeroDNI: number;
  respuestasSituacionSalud: any[] = [];
  slideOpts = { onlyExternal: false }
  constructor(
    private services: FormDataService,
    public user: UserDataService,
    public alertController: AlertController,
    public router: Router,
    public servicePaeAccess: ServicepaeaccessService
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {

    // this.user.setValorSituacionSalud(55);

    // this.getNumeroDNI();
    this.user.getNumeroDNI().then((valor) => {
      this.numeroDNI = valor;

      this.user.getValorSituacionSalud(this.numeroDNI).then(data => { //traer registro o autoev

        if (data != null) {
          this.valorBaseAutoEvaluacion = data;
          this.autoEvaluacion = true;
        } else {
          this.autoEvaluacion = false;

        }
        this.services.load().subscribe((data: any) => {
          if (this.autoEvaluacion) {
            this.respuestas = data.autoEvaluacion.preguntas;
          } else {
            this.respuestas = data.registro.preguntas;
            
          }
          
          console.log(data)
          this.cantidadPreguntas = this.respuestas.length;
          this.progressBar()

        })
      })
    });

    // this.getNombreUsuario(); // ? Se usa?

  }

  ionViewWillEnter() {

  }

  progressBar() {

    this.progressBarValue = this.preguntasRespondidas / this.cantidadPreguntas;
    // console.log(`${this.preguntasRespondidas} / ${this.cantidadPreguntas}`)
  }



  limpiarRespuestas(valorParam) {
    //console.log("CHECKLIMPIAR" + this.respuestas[0].respuestaMultiple)
    if (this.respuestas[0].respuestaMultiple) {  //Si permite 1 sola respuesta
      this.respuestas[0].respuestas.forEach(respuesta => {

        respuesta.valor == valorParam ? respuesta.seleccionada = true : respuesta.seleccionada = false;

      });
    } else {
      this.respuestas[0].respuestas.forEach(respuesta => { //Si permite multiples respuestas
        respuesta.valor == valorParam ? respuesta.seleccionada = !respuesta.seleccionada : 0;
      })

    }
  }


  


  getNombreUsuario() {

    this.user.getNombreUsuario().then((nombre) => {
      this.nombreUsuario = nombre.toLowerCase();
      console.log(this.nombreUsuario)

    })

  }

  async presentAlert(respuestasSituacionSalud) { //TODO: pasar mensaje por parametro
    this.respuestasSituacionSalud = respuestasSituacionSalud;
    debugger;
    const alert = await this.alertController.create({
      header: 'Atención',
      message: 'Esta seguro de envíar el formulario?',
      buttons: [{
        text: 'Cancelar',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Cancelo');
        }
      }, {
        text: 'Aceptar',
        cssClass: 'primary',
        handler: (bleh) => {
          if (this.autoEvaluacion) {
            this.calcularValorAutoEvaluacion();
          } else {
            this.setearSituacionSalud();
          }
        }
      }],

    });

    await alert.present();
  }

  calcularValorAutoEvaluacion() {
    let resultadoAutoEvaluacion = 0;
    console.log(this.respuestasSituacionSalud)
    debugger;
    this.respuestasSituacionSalud.forEach(respuesta => {

      if (respuesta.seleccionada == true) resultadoAutoEvaluacion += respuesta.valorAutoEvaluacion;
    });
    let resultadoFinal  =  resultadoAutoEvaluacion + this.valorBaseAutoEvaluacion;
    this.user.setValorAutoEvaluacion(resultadoFinal, this.numeroDNI);
    console.log('VALOR FINAAAAAAAAAAAL ')
    console.log(`${resultadoAutoEvaluacion} + ${this.valorBaseAutoEvaluacion}`)
    let fechaActual = new Date();
    console.log(fechaActual)
    this.user.SetFechaAutoEvaluacion(fechaActual, this.numeroDNI)
    console.log(fechaActual)
    this.servicePaeAccess.obtenerCodigo(fechaActual, resultadoFinal).then((data)=>{
      console.log(data);
    })

    this.router.navigate(["/inicio"]);


  }
  async setearSituacionSalud() {
    let valorFinal = 0;
    console.log(this.respuestasSituacionSalud)
    this.respuestasSituacionSalud.forEach(respuesta => {

      if (respuesta.valorSituacionSalud != null) valorFinal += respuesta.valorSituacionSalud;
    });

    if (await this.user.getMayorDeEdad()) {
      valorFinal += 0.5;
    }
    this.user.setValorSituacionSalud(valorFinal, this.numeroDNI);
    this.router.navigate(["/inicio"]); // , { replaceUrl: true }

    // this.user.setSituacionSalud(valorFinal)
  }

}
