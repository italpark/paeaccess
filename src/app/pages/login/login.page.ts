import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage'
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { datosLogin } from '../../interfaces/interfaces';
import { LoadingController } from '@ionic/angular';
import { ServicePaeAccessService } from 'src/app/providers/service-pae-access.service';
import { UserDataService } from 'src/app/providers/user-data.service';
import * as moment from 'moment';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('nroTramite') myInput;
  progressBar = 0;
  maxProgress = 7;
  preguntasRespondidas = 0;
  dni = '';
  datosUsuario: datosLogin = {};
  loading: any;
  constructor(private barcodeScanner: BarcodeScanner,
    private loadingCtrl: LoadingController,
    private service: ServicePaeAccessService,
    private router: Router,
    public storage: Storage,
    public user: UserDataService) { }

  ngOnInit() {
  }
  handleLogin(event) {
    // console.log(this.progressBar, event);
    this.preguntasRespondidas++;
    this.progressBar = this.preguntasRespondidas / this.maxProgress;
  }
  gotoNextField(event?) {
    // console.log(event);
    if (event.key === 'Enter') {
      // console.log("enter")
      // console.log(this.myInput)
      this.myInput.setFocus();
    }
    // if (event.key == "Unidentified") {
    //   let tempDNI=String(this.dni);
    //   this.dni = tempDNI.slice(0,-1)
    //   //console.log('period')

    // }
  }
  swallow() {
    // console.log("swallowed");
  }

  scanDNI() {
    this.barcodeScanner.scan({ formats: 'PDF_417' }).then(async barcodeData => {
      if (barcodeData.text != "") {
        // console.log('Barcode data', barcodeData);
        let datosTmp: Array<string>;
        let dniViejo: boolean;
        barcodeData.text.charAt(0) === '@' ? dniViejo = true : dniViejo = false;
        datosTmp = barcodeData.text.split('@');
        if (dniViejo) {

          // tslint:disable-next-line: radix
          this.datosUsuario.numeroDNI = parseInt(datosTmp[0]);
          this.datosUsuario.apellido = datosTmp[3];
          this.datosUsuario.nombre = datosTmp[4];
          this.datosUsuario.fechaNacimiento = datosTmp[6];
          this.datosUsuario.sexo = datosTmp[8];

        } else {
          // tslint:disable-next-line: radix
          this.datosUsuario.numeroDNI = parseInt(datosTmp[4]);
          this.datosUsuario.apellido = datosTmp[1];
          this.datosUsuario.nombre = datosTmp[2];
          this.datosUsuario.fechaNacimiento = datosTmp[6];
          this.datosUsuario.sexo = datosTmp[3];

        }

        let fechaNac = moment(this.datosUsuario.fechaNacimiento, "DD/MM/YYYY");

        let years = moment().diff(fechaNac, 'years');
        years > 60 ? this.user.setMayorDeEdad(true) : this.user.setMayorDeEdad(false);

        // Verificar tanto local como online la existencia del usuario
        // Online
        this.verificarUsuario();

        // Offline
        await this.user.login();

        await this.user.setDatosSituacionSalud(this.datosUsuario);
        await this.user.setNumeroDNI(this.datosUsuario.numeroDNI);
        await this.user.setNombreUsuario(this.datosUsuario.nombre);
        console.log(this.datosUsuario)


        console.log('cambio de pagina')
        // Cambio de pagina
        console.log(barcodeData)
        this.router.navigateByUrl('/inicio');
      }
      // console.log(this.datosUsuario)
    }).catch(err => {
      console.log('Error', err);
    });
  }


  verificarUsuario() {


    this.presentLoading('Espere un momento por favor...');
    this.service.login(this.datosUsuario).then((result: any) => {


      let token: string;
      result.token ? token = result.token : 0;
      localStorage.setItem("token", token);
      // this.loadingCtrl.dismiss();
      // console.log("funco", result);
      // this.router.navigateByUrl('/inicio');

    }, (err) => { console.log(err) });

  }
  async presentLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }
}
