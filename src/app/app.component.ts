import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { UserDataService } from './providers/user-data.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  loggedIn = false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public storage: Storage,
    public user: UserDataService
  ) {
    this.initializeApp();
  }
  async ngOnInit() {
    this.checkLoginStatus();
  }
  checkLoginStatus() {
    return this.user.isLoggedIn().then((loggedIn) => {
      console.log(loggedIn + 'loged');
      return this.updateLoggedInStatus(loggedIn);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.platform.backButton.subscribeWithPriority(9999, () => {
        document.addEventListener(
          'backbutton',
          function (event) {
            event.preventDefault();
            event.stopPropagation();
            console.log('hello');
          },
          false
        );
      });
      // this.storage.clear();
      // this.storage.keys().then(keis => {
      //   console.log(keis)
      // })

      // this.statusBar.overlaysWebView(true);
      // set status bar to white
      // this.statusBar.backgroundColorByHexString('#ffffff');
      this.splashScreen.hide();
    });
  }

  updateLoggedInStatus(loggedIn: boolean) {
    setTimeout(() => {
      this.loggedIn = loggedIn;
      console.log(this.loggedIn);
      if (this.loggedIn) {
        this.router.navigateByUrl('/inicio');
      } else {
        this.router.navigateByUrl('/login');
      }
    }, 300);
  }
}
